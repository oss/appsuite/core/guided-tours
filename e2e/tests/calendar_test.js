/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Client Onboarding > Tours')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Calendar Tour', async ({ I }) => {
  await I.login('app=io.ox/calendar')

  // Open tour dialog
  I.click('~Help')
  I.clickDropdown('Guided tour for this app')
  I.waitForVisible('.wizard-step')

  I.waitForText('Creating a new appointment')
  I.click('Start tour')

  I.waitForText("Entering the appointment's data")
  I.click('Next')

  I.waitForText('Creating recurring appointments')
  I.click('Next')

  I.waitForText('Inviting other participants')
  I.click('Next')

  I.waitForText('Using the reminder function')
  I.click('Next')

  I.waitForText('Adding attachments')
  I.click('Next')

  I.waitForText('Creating the appointment')
  I.seeElement('.hotspot')
  I.click('Next')

  I.waitForText('Selecting a view')
  I.click('Next')

  I.waitForText('The calendar views')
  I.click('Next')

  I.waitForText('The List view')
  I.click('Finish')

  I.waitForInvisible('.wizard-step', 5)
})
