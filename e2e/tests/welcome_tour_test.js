/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Client Onboarding > Tours')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('[C303317] Welcome Tour', async ({ I }) => {
  await I.login('app=io.ox/mail')

  // Open tour dialog
  I.click('~Help')
  I.clickDropdown('Getting started')
  I.waitForVisible('.getting-started-tour')

  I.waitForText('Start tour')
  I.click('Start tour')
  I.waitForText('App launcher')

  // Check abort dialog
  I.click('~Close', '.wizard-step')
  I.waitForText('Cancel tour')
  I.click('Finish')
  I.waitForInvisible('.wizard-step', 5)

  // Restart Tour
  I.click('~Help')
  I.clickDropdown('Getting started')

  // Wait for dialog to open
  I.waitForVisible('.getting-started-tour')
  I.waitForText('Start tour')
  I.click('Start tour')

  // Check correct Step is displayed 1/4 - Welcome OX App suite
  I.waitForText('App launcher')
  I.click('Next')

  // Check correct Step is displayed 3/4 - Personal Settings
  I.waitForText('Settings menu')
  I.seeElement('.hotspot')
  I.click('Next')

  // Check correct Step is displayed 4/4 - Settings
  I.waitForText('Search in settings')
  I.seeElement('.hotspot')
  I.click('Next')

  // Check correct Step is displayed 5/4 - Help
  I.waitForText('Help menu')
  I.seeElement('.hotspot')
  I.click('Finish')

  // Check that wizard was closed
  I.waitForInvisible('.wizard-step', 5)
})
