/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Client Onboarding > Tours')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Tasks Tour', async ({ I }) => {
  await I.login('app=io.ox/tasks')

  I.click('~Help')
  I.click('Guided tour for this app')
  I.waitForElement('.wizard-container', 5)

  I.waitForText('Creating a new task')
  I.click('Start tour')

  I.waitForText('Entering the task\'s data')
  I.click('Next')

  I.waitForText('Adding further details')
  I.click('Next')

  I.waitForText('Creating recurring tasks')
  I.click('Next')

  I.waitForText('Using the reminder function')
  I.click('Next')

  I.waitForText('Tracking the editing status')
  I.click('Next')

  I.waitForText('Inviting other participants')
  I.click('Next')

  I.waitForText('Entering billing information')
  I.click('Next')

  I.waitForText('Creating the task')
  I.click('Next')

  I.waitForText('Sorting tasks')
  I.click('Next')

  I.waitForText('Editing multiple tasks')
  I.click('Finish')

  // Check that wizard was closed
  I.waitForInvisible('.wizard-step', 5)
})
