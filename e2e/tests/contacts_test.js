/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Client Onboarding > Tours')

Before(async ({ users }) => { await users.create() })

After(async ({ users }) => { await users.removeAll() })

Scenario('Contacts Tour', async ({ I }) => {
  await I.login('app=io.ox/contacts')

  I.click('~Help')
  I.click('Guided tour for this app')
  I.waitForElement('.wizard-container', 5)

  I.waitForText('Creating a new contact')
  I.click('Start tour')

  I.waitForText('Navigation bar')
  I.click('Next')

  I.waitForText('Sending an email to a contact')
  I.click('Next')

  I.waitForText('Selecting multiple contacts')
  I.click('Finish')

  // Check that wizard was closed
  I.waitForInvisible('.wizard-step', 5)
})
