/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

Feature('Client Onboarding > Tours')

Before(async ({ users }) => {
  await users.create()
  await users[0].context.hasCapability('multifactor')
})

After(async ({ users }) => {
  await users[0].context.doesntHaveCapability('multifactor')
  await users.removeAll()
})

Scenario.skip('Multifactor Tour', async ({ I }) => {
  await I.login('app=io.ox/settings', { id: 'io.ox/multifactor' })

  I.waitForText('Second Factor Authentication')
  I.click('Start tour')

  I.waitForText('You can now add additional verification options')
  I.click('Next')

  I.waitForText('Adding verification option')
  I.click('Next')

  I.waitForText('Adding a code via text message')
  I.click('Next')

  I.waitForText('Authentication Requests')
  I.click('Next')

  I.waitForText('Removing')
  I.click('Finish')

  I.waitForInvisible('.wizard-step', 5)
})
