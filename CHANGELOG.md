# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [8.6.12] - 2025-01-16

### Changed

- Various dependency updates


## [8.6.11] - 2024-12-20

### Fixed

- Import error in portal tour (#35) [`59f39ea1`](https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/-/commit/59f39ea1499427a90b80e4721b59a3c68ee0a214)

## [8.6.10] - 2024-11-22

### Changed

- Various dependency updates



## [8.6.9] - 2024-10-24

### Changed

- Various dependency updates

### Fixed

- Consistently use the German term for "App Launcher" throughout all components (appsuite/web-apps/translations/#100)


## [8.6.8] - 2024-09-23

### Changed

- Various dependency updates


## [8.6.7] - 2024-08-28

### Changed

- Various dependency updates


## [8.6.6] - 2024-08-02

### Changed

- Various dependency updates

## [8.6.5] - 2024-07-04

### Changed

- Various dependency updates


## [8.6.4] - 2024-06-07

### Changed

- Helm: Make security context configuration overwritable [`3d0576a`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/3d0576ada21fc18d0e1f35e79584e829ca7d3b26)

## [8.6.3] - 2024-03-08

### Added

- Latest translations

### Fixed

- OXUIB-2739: Guided tour start screen missing some it_IT translations [`f6771e5`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/f6771e5b585c83389d0270fb242f7f350dc7c2f9)


## [8.6.2] - 2024-02-09

### Fixed

- [`OXUIB-2714`](https://jira.open-xchange.com/browse/OXUIB-2714): Settings menu dropdown not closing automatically [`cb505a1`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/cb505a16365f88eef11cd79425804794fe1f662b)

## [8.6.1] - 2024-01-05

### Added

- latest translations

## [8.6.0] - 2023-10-27

### Changed

- [`OXUI-1367`](https://jira.open-xchange.com/browse/OXUI-1367): Improve general guided tour through the app [`e4c67f6`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/e4c67f6c5d31645376633bc212140563b68b5164)

## [8.5.2] - 2023-09-01

### Removed

- Pending removals for 8.8 [`f405947`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/f405947dd642043eac68ba4343472be5b0289c79)

### Fixed

- [`OXUIB-2483`](https://jira.open-xchange.com/browse/OXUIB-2483): Disabled account dropdown breaks "Getting started" guided tours [`b370667`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/b370667919ac309659e8625cd7ee928ae5fa2d07)

## [8.5.1] - 2023-08-04

### Fixed

- [`OXUIB-2430`](https://jira.open-xchange.com/browse/OXUIB-2430): Guided Tour for 2FA breaks when there is no SMS provider [`79eb6ae`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/79eb6aebdbe7ab623fece0e3ba346811d63eaabf)

## [8.5.0] - 2023-04-06

### Fixed

- Guided tour for calendar [`ddf862e`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/ddf862e877bd4424a43f1bdf2acb24608276c1c2)

## [8.4.3] - 2023-03-10

### Fixed

- [`OXUIB-2201`](https://jira.open-xchange.com/browse/OXUIB-2201): Guided tour for 2-Factor-Auth broken [`4dbbef2`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/4dbbef246cf108edbe6e34b98f95f4df369e65ec)


## [8.4.2] - 2022-12-16

### Added

- Every request contains the version header for easier recognition of version updates

### Changed

- Update dependencies


## [8.4.1] - 2022-11-11

### Deprecated

- [`OXUI-1057`](https://jira.open-xchange.com/browse/OXUI-1057): Remove guided tour for Settings
[`e2ba6d5`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/e2ba6d5d70ff205085661f0798dc462eb2b12e8a)

### Removed

- Close and open settings code and replace w/ core code
[`4abbf36`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/4abbf3604e797eb10b65f078bf1d4f188c63017c)

### Fixed

- [`OXUIB-1797`](https://jira.open-xchange.com/browse/OXUIB-1797), [`GUARD-350`](https://jira.open-xchange.com/browse/GUARD-350), [`OXUIB-1926`](https://jira.open-xchange.com/browse/OXUIB-1926) [`209e055`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/209e055b61516d250c17d2a0138e7f02cb8ec9c3)
  - Checked in all latest deliveries again
  - Different typos in Dutch guided tours - Multifactor Authentication
- [`OXUIB-1922`](https://jira.open-xchange.com/browse/OXUIB-1922): Guided tour highlighting is missing
[`63ef430`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/63ef43090f67d67a6ef47bf6b9bd4cf8843ab37d)
- [`OXUIB-2045`](https://jira.open-xchange.com/browse/OXUIB-2045): Guided tour for Mail App breaks mail sort menu
[`a7c810f`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/a7c810f9e2bbc2bd015869d564a59128325c5b28)
- Close guard password prompt to avoid interference with guard
[`13f2fee`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/13f2feed28210581c42c12f1a0fcd84eaa74adcd)


## [8.4.0] - 2022-09-02

### Changed

- Update vite to latest version

### Fixed

- [`OXUIB-1871`](https://jira.open-xchange.com/browse/OXUIB-1871): Guided Tour for Portal app does not finish [`1c8d0c7`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/1c8d0c7e3a24b4d4489b68a959a218bd9d4dc135)

## [8.3.1] - 2022-06-21

### Added

- Integrate translations [`4306d19`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/4306d19e4105421983e19868b0d545d8d0f3aed6)

## [8.3.0] - 2022-06-21

### Added

- Add standard labels to pod [`db8a66c`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/db8a66c78375ac188e1693b53072611a8b5c1dec)
- [`OXUI-1006`](https://jira.open-xchange.com/browse/OXUI-1006) Configure dependabot for guided tours [`749a8e4`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/749a8e4c69b4620c1637d17b46c37216734999d5)

### Removed

- Remove superfluous resource definitions [`702f7bb`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/702f7bb8317d3409d4dbd8b4b07628a51672fde5)
- Remove default assignee in dependabot [`f579ee3`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/f579ee39ef4eb7251df41c1fed5adf405196ad21)

## [8.2.1] - 2022-05-09

### Fixed

- Fix preview deployment [`835ae32`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/835ae32bedcf9e35a9bffa76c611968ac937033e)

## [8.2.0] - 2022-04-29

### Removed

- removed fuzzy flag in German file [`8632e77`](https://gitlab.open-xchange.com/frontend/guidedtours/commit/8632e77c66d26c819677fc240e9dad0a146f959c)

### Fixed

- Issues in Finnish translation
## [8.1.0] - 2022-03-28

### Fixed
- OXUIB-1511: MFA tour refers to SMS which is a bad example when disabled - fixed in-house for most languages (not for: ja, zh*, tr), needs to be checked by translators
- OXUIB-1478: Improve css selectors to work for all languages

## [8.0.236244] - 2022-02-10
- Add release workflow
## [8.0.236243]

### Added
- Start changelog


[unreleased]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.12...main
[8.6.2]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.6.1...8.6.2
[8.6.1]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.6.0...8.6.1
[8.6.0]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.5.2...8.6.0
[8.5.2]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.5.1...8.5.2
[8.5.1]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.5.0...8.5.1
[8.5.0]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.4.3...8.5.0
[8.4.3]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.4.2...8.4.3
[8.4.2]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.4.1...8.4.2
[8.4.1]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.4.0...8.4.1
[8.4.0]: https:///git@gitlab.open-xchange.com:frontend/guidedtours/compare/8.3.1...8.4.0
[8.3.1]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.3.0...8.3.1
[8.3.0]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.2.1...8.3.0
[8.2.1]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.2.0...8.2.1
[8.2.0]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.1.0...8.2.0
[8.1.0]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.0.236244...8.1.0
[8.0.236244]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/null...8.0.236244

[8.6.12]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.11...8.6.12
[8.6.11]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.10...8.6.11
[8.6.10]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.9...8.6.10
[8.6.9]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.8...8.6.9
[8.6.8]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.7...8.6.8
[8.6.7]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.6...8.6.7
[8.6.6]: https://gitlab.open-xchange.com/appsuite/web-apps/guidedtours/compare/8.6.5...8.6.6
[8.6.5]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.6.4...8.6.5
[8.6.4]: https://gitlab.open-xchange.com/frontend/guidedtours/compare/8.6.3...8.6.4
