/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '$/jquery'
import ox from '$/ox'
import moment from '$/io.ox/core/moment'

import Tour from '$/io.ox/core/tk/wizard'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'

import gt from 'gettext'

let createApp

/* Tour: calendar / appointments */
Tour.registry.add({
  id: 'default/io.ox/calendar',
  app: 'io.ox/calendar',
  priority: 1
}, function () {
  const tour = new Tour()

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', () => restoreFloatingWindows(openFloatingWindows))

  tour.step()
    .title(gt('Creating a new appointment'))
    .content(gt('To create a new appointment, click on New appointment in the toolbar.'))
    .spotlight('.primary-action', { position: 'left' })
    .hotspot('.primary-action button', { position: 'left' })
    .on('before:show', function () {
      if (createApp && !createApp.getWindow().floating.model.get('minimized')) {
        createApp.getWindow().floating.onMinimize()
      }
    })
    .on('next', async function () {
      if (createApp) {
        if (createApp.getWindow().floating.model.get('minimized')) createApp.getWindow().floating.model.set('minimized', false)
        return
      }

      const [{ default: edit }, { default: models }] = await ox.load(() => Promise.all([import('$/io.ox/calendar/edit/main'), import('$/io.ox/calendar/model')]))
      const app = edit.getApp()
      createApp = app
      await app.launch()

      const refDate = moment().startOf('hour').add(1, 'hours')

      app.create(new models.Model({
        folder: app.folder.get(),
        startDate: { value: refDate.format('YYYYMMDD[T]HHmmss'), tzid: refDate.tz() },
        endDate: { value: refDate.add(1, 'hours').format('YYYYMMDD[T]HHmmss'), tzid: refDate.tz() }
      }))
    })
    .end()
    .step()
    .title(gt('Entering the appointment\'s data'))
    .content(gt('Enter the subject, the start and the end date of the appointment. Other details are optional.'))
    .waitFor('.io-ox-calendar-edit-window.active [data-extension-id="title"] > label')
    .spotlight('.io-ox-calendar-edit-window.active [data-extension-id="title"] > label')
    .on('before:show', function () {
      if ($('.io-ox-calendar-edit-window.active [data-extension-id="title"] > label').length === 0) return
      $('.io-ox-calendar-edit-window.active [data-extension-id="title"] > label')[0].scrollIntoView()
    })
    .on('show', function () {
      $('.io-ox-calendar-edit-window.active [data-extension-id="title"] > label')[0].scrollIntoView()
    })
    .end()
    .step()
    .title(gt('Creating recurring appointments'))
    .content(gt('To create recurring appointments, enable Repeat. Functions for setting the recurrence parameters are shown.'))
    .spotlight('.io-ox-calendar-edit-window.active [data-extension-id="recurrence"]')
    .on('before:show', function () {
      $('.io-ox-calendar-edit-window.active [data-extension-id="recurrence"]')[0].scrollIntoView()
    })
    .end()
    .step()
    .title(gt('Inviting other participants'))
    .content(gt('To invite other participants, enter their names in the field below Participants. To avoid appointment conflicts, click on Find a free time at the upper right side.'))
    .spotlight('.io-ox-calendar-edit-window.active .add-participant')
    .on('before:show', function () {
      $('.io-ox-calendar-edit-window.active .add-participant:last')[0].scrollIntoView()
    })
    .end()
    .step()
    .title(gt('Using the reminder function'))
    .content(gt('To not miss the appointment, use the reminder function.'))
    .spotlight('.io-ox-calendar-edit-window.active .alarms-link-view')
    .on('before:show', function () {
      $('.io-ox-calendar-edit-window.active [data-extension-id="alarms-container"]')[0].scrollIntoView()
    })
    .end()
    .step()
    .title(gt('Adding attachments'))
    .content(gt('Further down you can add documents as attachments to the appointment.'))
    .spotlight('.attachments-form .btn-file')
    .on('before:show', function () {
      $('.io-ox-calendar-edit-window.active [data-extension-id="attachments_legend"]:last')[0].scrollIntoView()
    })
    .end()
    .step()
    .title(gt('Creating the appointment'))
    .content(gt('To create the appointment, click on Create at the lower left side.'))
    .referTo('.io-ox-calendar-edit-window.active [data-action="save"]')
    .hotspot('.io-ox-calendar-edit-window.active [data-action="save"]')
    .spotlight('.io-ox-calendar-edit-window.active [data-action="save"]')
    .on('before:show', function () {
      if (createApp && createApp.getWindow().floating.model.get('minimized')) {
        createApp.getWindow().floating.model.set('minimized', false)
      }
    })
    .end()
    .step()
    .title(gt('Selecting a view'))
    .content(gt('To select one of the views like Day, Month or List, click on View in the toolbar. Select a menu entry from the Layout section.'))
    .navigateTo(() => import('$/io.ox/calendar/main'))
    .spotlight('.calendar-header .dropdown', { position: 'left' })
    .referTo('.calendar-header .dropdown')
    .waitFor('.calendar-header .dropdown')
    .on('wait', function () {
      if (createApp && !createApp.getWindow().floating.model.get('minimized')) {
        createApp.getWindow().floating.onMinimize()
      }
      $('.calendar-header li.dropdown ul').css('display', 'block')
    })
    .on('hide', function () {
      $('.calendar-header li.dropdown ul').css('display', '')
    })
    .end()
    .step()
    .title(gt('The calendar views'))
    .content(gt('The calendar views display a calendar sheet with the appointments for the selected time range.'))
    .spotlight('.calendar-header .dropdown ul div:has(a[data-name="layout"])')
    .referTo('.calendar-header .dropdown ul')
    .waitFor('.calendar-header .dropdown ul div:has(a[data-name="layout"])')
    .on('wait', function () {
      $('.calendar-header .dropdown ul').css('display', 'block')
    })
    .on('hide', function () {
      $('.calendar-header .dropdown ul').css('display', '')
    })
    .end()
    .step()
    .title(gt('The List view'))
    .content(gt('The List view shows a list of the appointments in the current folder. If clicking on an appointment, the appointment\'s data and some functions are displayed in the Detail view.'))
    .hotspot('.calendar-header .dropdown ul a[data-value="list"]')
    .spotlight('.calendar-header .dropdown ul a[data-value="list"]')
    .referTo('.calendar-header .dropdown ul')
    .waitFor('.calendar-header .dropdown ul a[data-value="list"]')
    .on('wait', function () {
      $('.calendar-header .dropdown ul').css('display', 'block')
    })
    .on('hide', function () {
      $('.calendar-header .dropdown ul').css('display', '')
    })
    .end()
    .on('stop', function () {
      if (createApp) {
        createApp.quit()
        createApp = null
      }
    })
    .start()
})
