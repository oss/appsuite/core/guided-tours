/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '$/jquery'

import capabilities from '$/io.ox/core/capabilities'
import Tour from '$/io.ox/core/tk/wizard'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'
import ext from '$/io.ox/core/extensions'

import gt from 'gettext'

/* Tour: intro. The special one that does not belong to an app */
Tour.registry.add({
  id: 'default/io.ox/intro'
}, function () {
  // Tour needs webmail and should be disabled for guests (See Bug 40545)
  if (!capabilities.has('webmail && !guest')) return
  let showAbortDialog = true
  const tour = new Tour({ showStepNumbers: true })

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', () => restoreFloatingWindows(openFloatingWindows))

  tour.step({ noAutoAlign: true })
  // #. %s is the product name, e.g. OX App Suite
    .title('')
    .content('')
    .on('before:show', function () {
      this.$el.addClass('getting-started-tour getting-started-modal')
      this.$('.wizard-header').addClass('getting-started-header')
      this.$('.wizard-header .wizard-close').remove()

      const flexContainer = $('<div class="getting-started-container"></div>')
      flexContainer.append($('<div class="getting-started-brush"></div>').append($('<img>').attr('src', 'themes/default/assets/tour_theme_icon.svg')))
      flexContainer.append($('<div class="getting-started-text"></div>')
        .append($('<h2 class="greeting-title"></h2>').text(gt('Welcome')))
        .append($('<p></p>').text(gt('In this cutting-edge release, we\'re proud to unveil a brand-new user interface design, a broad selection of fresh themes including an elegant dark mode, and a plethora of new powerful features.')))
        .append($('<p class="mt-16"></p>').text(gt('Take a brief product tour to get yourself oriented!'))))
      this.$('.wizard-content').append(flexContainer)

      this.$('.wizard-footer').empty()
      this.$('.wizard-footer').addClass('modal-footer')
      this.$('.wizard-footer').append($('<button type="button" class="btn btn-default" data-action="close" aria-label="Close"></button>').text(gt('Cancel')))
      this.$('.wizard-footer').append($('<button type="button" class="btn btn-primary" data-action="next"></button>').text(gt('Start tour')))
    })
    .end()

  tour.step({ back: false, noAutoAlign: true })
  // #. %s is the product name, e.g. OX App Suite
    .title(gt('App launcher'))
    .content(gt('Your main navigation menu to switch between apps.'))
    .hotspot('#io-ox-launcher', { top: 22, left: 12 })
    .spotlight('.launcher-dropdown')
    .waitFor('.launcher-dropdown')
    .on('wait', function () {
      $('#io-ox-launcher .launcher-btn').click()
      $('#io-ox-launcher').attr('forceOpen', true)
    })
    .on('hide', function () {
      $('#io-ox-launcher').attr('forceOpen', false)
      $('#io-ox-launcher .launcher-btn').click()
    })
    // Due to the first modal-like dialog without "Step", we need to decrement the next pages by 1.
    .on('before:show', function () {
      this.$el.addClass('getting-started-card getting-started-tour')
      this.$('.wizard-step-number').text(`${gt('Step')} 1/4`)
    })
    .end()

  if (ext.point('io.ox/core/appcontrol/right').isEnabled('account')) {
    tour.step({ back: false, noAutoAlign: true })
      .title(gt('Settings menu'))
      .content(gt('Change the most important settings for each app directly in the dropdown or open all settings from here.'))
      .hotspot('#io-ox-topbar-settings-dropdown-icon', { top: 22, left: 16 })
      .spotlight('#topbar-settings-dropdown')
      .waitFor('#io-ox-topbar-settings-dropdown-icon')
      .on('wait', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', true) })
      .on('before:show', function () {
        this.$el.addClass('getting-started-card getting-started-tour')
        this.$('.wizard-step-number').text(`${gt('Step')} 2/4`)
      })
      .end()
  }

  tour.step({ back: false, noAutoAlign: true })
    .title(gt('Search in settings'))
    .content(gt('You can access all settings here. To quickly find what you need, try the new search function.'))
    .hotspot('.io-ox-settings-main .search-container', { top: 22, left: 16 })
    .spotlight('.io-ox-settings-main .search-container')
    .waitFor('.io-ox-settings-main .search-container')
    .on('wait', function () { $('a[data-name="settings-app"]').click(); $('a[data-name="settings-app"]').attr('forceOpen', true) })
    .on('hide', function () { $('button.close-settings').click(); $('a[data-name="settings-app"]').attr('forceOpen', false) })
    .on('before:show', function () {
      $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click()
      this.$el.addClass('getting-started-card getting-started-tour')
      this.$('.wizard-step-number').text(`${gt('Step')} 3/4`)
    })
    .end()
    .step({ back: false, noAutoAlign: true })
    .title(gt('Help menu'))
    .content(gt('If you need further guidance, you can find a comprehensive user guide, guided tours and the latest updates in the help menu.'))
    .hotspot('#io-ox-topbar-help-dropdown-icon', { top: 22, left: 12 })
    .spotlight('#topbar-help-dropdown')
    .on('show', function () { showAbortDialog = false })
    .on('before:show', function () { $('#io-ox-topbar-help-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-help-dropdown-icon').attr('forceOpen', true) })
    .on('hide', function () { $('#io-ox-topbar-help-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-help-dropdown-icon').attr('forceOpen', false) })
    .on('before:show', function () {
      this.$el.addClass('getting-started-card getting-started-tour')
      this.$('.wizard-step-number').text(`${gt('Step')} 4/4`)
    })
    .end()
    .on('stop', function () {
      if (showAbortDialog) {
        new Tour()
          .step()
          .title(gt('Cancel tour'))
          // #. %s is the "getting started" tour button label
          .content(gt('You can restart this tour at any time by clicking on the help icon and choose "%s".',
            // #. Tour name; general introduction
            gt('Getting started')))
          .on('before:show', function () {
            this.$el.addClass('getting-started-card getting-started-tour')
          })
          .end()
          .start()
      }
    })
    .start()
})
