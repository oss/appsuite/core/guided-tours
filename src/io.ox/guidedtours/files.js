/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import ox from '$/ox'
import $ from '$/jquery'
import _ from '$/underscore'

import Tour from '$/io.ox/core/tk/wizard'
import api from '$/io.ox/files/api'
import capabilities from '$/io.ox/core/capabilities'
import { settings } from '$/io.ox/core/settings'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'

import gt from 'gettext'

// import core dictionary so we only have to translate product names one time
import { dictionaries } from '$/gettext'
// just in case use default gt as fallback to prevent runtime errors
const gtCore = dictionaries['io.ox/core'] || gt

/* Tour: files / ... */
Tour.registry.add({
  id: 'default/io.ox/files',
  app: 'io.ox/files',
  priority: 1
}, function () {
  const SAMPLE_CONTENT = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'
  const blob = new window.Blob([SAMPLE_CONTENT], { type: 'text/plain' })
  const standardFolder = settings.get('folder/infostore')
  let file
  let key
  let prevState
  const current = {}
  const tour = new Tour()

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', () => restoreFloatingWindows(openFloatingWindows))

  function cleanup () {
    api.get(file).done(function (newData) {
      api.remove([newData])
      api.trigger('refresh.all')
    })
    // restore
    const app = ox.ui.App.getCurrentApp()
    app.folder.set(current.folder)
    app.props.set('layout', current.view)
  }

  // define as function or key is undefined
  function getSelector () {
    return '.list-view li[data-cid="' + key + '"]'
  }

  tour.step()
    // #. %1$s is usually "Drive" (product name; might be customized)
    .title(gt('The %1$s app', gtCore.pgettext('app', 'Drive')))
    .content(gt('Welcome to your cloud storage app. This Guided Tour will introduce you to your new online storage solution - your one point to access online stored files from all your accounts. This is where you can upload and save your files, share them and synchronize them with different devices.'))
    .on('wait', function () {
      if ($('.launcher-dropdown:visible').length === 0) $('#io-ox-launcher .dropdown-toggle').click()
      $('#io-ox-launcher').attr('forceOpen', true)
    })
    .on('hide', function () {
      $('#io-ox-launcher').attr('forceOpen', false)
      if ($('.launcher-dropdown:visible').length) $('#io-ox-launcher .dropdown-toggle').click()
    })
    .waitFor('.launcher-dropdown:visible')
    .hotspot('.launcher-dropdown [data-app-name="io.ox/files"]')
    .spotlight('.launcher-dropdown [data-app-name="io.ox/files"]')
    .on('close', cleanup)
    .end()
    .step()
    .title(gt('Folder tree'))
    .content(gt('On the left you can see the folder tree. It displays your folder structure and allows you to navigate to specific folders and subfolders. To make your life easier, we have already included folders for your Documents, Music, Pictures and Videos.'))
    .on('before:show', function () {
      if ($('.folder-tree:visible').length === 0) {
        $('.generic-toolbar.bottom a').click()
      }
    })
    .waitFor('.folder-tree:visible')
    .hotspot('li[data-model="virtual/drive/private"] .folder-icon')
    .spotlight('li[data-model="virtual/drive/private"] .folder-icon')
    .end()
    .step()
    .title(gt('Folder content'))
    .content(gt('Clicking on a folder displays all the subfolders, documents, media and other files that it contains.'))
    .waitFor(getSelector)
    .spotlight('.list-view', { position: 'left' })
    .end()
    .step()
    .title(gt('Select a view'))
    .content(gt('Different views are available. Just select the one you like best.'))
    .spotlight('#topbar-settings-dropdown div:has([data-name="layout"])', { position: 'left' })
    .hotspot('#io-ox-topbar-settings-dropdown-icon', { top: 22, left: 16 })
    .on('before:show', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', true) })
    .on('hide', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', false) })
    .end()
    .step()
    .title(gt('Toolbar'))
    .content(gt('At the top left you can find a button with many functions and additional options. You can easily create new folders, new files and much more.'))
    .spotlight('.primary-action', { position: 'left' })
    .hotspot('.primary-action button', { position: 'left' })
    .on('before:show', function () {
      prevState = $('.primary-action .dropdown ul').get(0).style.display
      $('.primary-action .dropdown ul').css('display', 'block')
    })
    .on('hide', function () { $('.primary-action .dropdown ul').css('display', prevState) })
    .end()
    .step()
    .title(gt('Upload a new file'))
    // #. %1$s is usually "Drive" (product name; might be customized)
    .content(gt('To upload a new file from your local device, simply click on "Upload files" or "Upload folder" and select the file or folder you would like to upload. It is even easier if you just drag and drop files from your local device into %1$s. The uploaded file is now available in %1$s on all your devices.', gtCore.pgettext('app', 'Drive'))).spotlight('.classic-toolbar-container [data-action="io.ox/files/actions/upload"]', { position: 'left' })
    .spotlight('a[data-name="io.ox/files/actions/upload"]', { position: 'left' })
    .hotspot('a[data-name="io.ox/files/actions/upload"]', { position: 'left' })
    .on('before:show', function () {
      prevState = $('.primary-action .dropdown ul').get(0).style.display
      $('.primary-action .dropdown ul').css('display', 'block')
    })
    .on('hide', function () { $('.primary-action .dropdown ul').css('display', prevState) })
    .end()
    .step()
    .title(gt('Preview files'))
    .content(gt('Clicking on the view icon leads you to a preview of the selected file.'))
    .on('before:show', function () {
      if (_.device('touch')) return $('.list-view li[data-cid="' + key + '"]').tap()
      $('.list-view li[data-cid="' + key + '"]').click()
    })
    .waitFor('.classic-toolbar-container [data-action="io.ox/files/actions/viewer"]')
    .spotlight('.classic-toolbar-container [data-action="io.ox/files/actions/viewer"]', { position: 'right' })
    .hotspot('.classic-toolbar-container [data-action="io.ox/files/actions/viewer"] svg', { position: 'right' })
    .end()
    .step()
    .title(gt('Preview mode'))
    .content(gt('From preview you can also select other options to help you manage and work on your files.'))
    .on('before:show', function () {
      $('.classic-toolbar-container [data-action="io.ox/files/actions/viewer"]').click()
    })
    .on('hide', function () {
      $('.viewer-toolbar [data-action="io.ox/core/viewer/actions/toolbar/close"]').click()
    })
    .waitFor('.io-ox-viewer .viewer-toolbar [data-action="io.ox/files/actions/editor"]')
    .hotspot('.viewer-toolbar [data-action="io.ox/files/actions/editor"]', { position: 'bottom' })
    .spotlight('.viewer-toolbar [data-action="io.ox/files/actions/editor"]')
    .end()

  if (capabilities.has('invite_guests') && capabilities.has('share_links')) {
    tour.step()
      .title(gt('Share files'))
      .content(gt('Here you can share files with your colleagues and external contacts. You can also collaborate on a document and set different rights.'))
      .spotlight('.classic-toolbar-container [data-action="io.ox/files/actions/share"]', { position: 'left' })
      .hotspot('.classic-toolbar-container [data-action="io.ox/files/actions/share"]', { position: 'left' })
      .end()
      .step()
      .title(gt('Sharing option - Invite people'))
      .content(gt('Choose from two alternatives to share your files and folders. Use "Invited people only" if you want to manage access rights and allow recipients to create and edit files. Internal and external participants are also able to collaborate with you on documents at the same time.'))
      .on('before:show', function () {
        if ($('.share-permissions-dialog .modal-dialog').length === 0) {
          $('.classic-toolbar-container [data-action="io.ox/files/actions/share"]').click()
        } else {
          $('.share-permissions-dialog .modal-dialog .access-select select option[value="1"]').attr('selected', null)
          $('.share-permissions-dialog .modal-dialog .access-select select option[value="0"]').attr('selected', 'selected')
        }
      })
      .waitFor('.share-permissions-dialog .modal-dialog .access-select select')
      .spotlight('.share-permissions-dialog .modal-dialog .access-select select', { position: 'right' })
      .hotspot('.share-permissions-dialog .modal-dialog .access-select select', { position: 'left' })
      .on('back', function () {
        $('.share-permissions-dialog .modal-dialog [data-action="abort"]').click()
      })
      .on('close', function () {
        $('.share-permissions-dialog .modal-dialog [data-action="abort"]').click()
      })
      .end()
      .step()
      .title(gt('Sharing option - Everyone who has the link'))
      .content(gt('It is also possible to get a sharing link to let others view or download your files. You can use an expiration date and password protection if you like.'))
      .waitFor('.share-permissions-dialog .modal-dialog .access-select select')
      .on('show', function () {
        $('.share-permissions-dialog .modal-dialog .access-select select option[value="0"]').attr('selected', null)
        $('.share-permissions-dialog .modal-dialog .access-select select option[value="1"]').attr('selected', 'selected')
      })
      .spotlight('.share-permissions-dialog .modal-dialog .access-select select', { position: 'right' })
      .hotspot('.share-permissions-dialog .modal-dialog .access-select select', { position: 'left' })
      .on('before:show', function () {
        if ($('.share-permissions-dialog .modal-dialog').length === 0) {
          $('.classic-toolbar-container [data-action="io.ox/files/actions/share"]').click()
        } else {
          $('.share-permissions-dialog .modal-dialog .access-select select option[value="1"]').attr('selected', null)
          $('.share-permissions-dialog .modal-dialog .access-select select option[value="0"]').attr('selected', 'selected')
        }
      })
      .on('close', function () {
        $('.share-permissions-dialog .modal-dialog [data-action="abort"]').click()
      })
      .on('next', function () {
        $('.share-permissions-dialog .modal-dialog [data-action="abort"]').click()
      })
      .end()
  }

  if (capabilities.has('text') && capabilities.has('spreadsheet')) {
    tour.step()
      .title(gt('Edit documents'))
      // #. %1$s is usually "Drive" (product name; might be customized)
      .content(gt('Did you know that you can edit text documents and spreadsheets online? %1$s will automatically update your edited file, but thanks to versioning the original file stays available.', gtCore.pgettext('app', 'Drive')))
      .spotlight('.classic-toolbar-container [data-action="io.ox/files/actions/editor"]', { position: 'right' })
      .hotspot('.classic-toolbar-container [data-action="io.ox/files/actions/editor"]', { position: 'right' })
      .on('back', function () {
        $('.classic-toolbar-container [data-action="io.ox/files/actions/invite"]').click()
      })
      .end()
  }

  tour.step()
    .title(gt('File details'))
    .content(gt('The file details side bar offers additional information about your files. Just enable the File details option from the Settings drop down menu and select a file to see the details.'))
    .on('before:show', function () {
      if ($('.classic-toolbar-container [data-action="io.ox/files/actions/invite"]').closest('.dropdown.open').length > 0) {
        $('.classic-toolbar-container [data-action="io.ox/files/actions/invite"]').closest('.dropdown').removeClass('open').attr('forceOpen', false)
      }
      if ($('.viewer-sidebar:visible').length === 0) {
        $('.classic-toolbar-container [data-dropdown="view"] li [data-name="details"]').click()
      }
    })
    .spotlight('.viewer-sidebar', { position: 'left' })
    .hotspot('.viewer-sidebar', { position: 'left' })
    .end()

  if (Object.keys(capabilities.get()).find(e => e.startsWith('filestorage'))) {
    tour.step()
      .title(gt('Add another account'))
      // #. %1$s is usually "Drive" (product name; might be customized)
      .content(gt('%1$s allows you to connect to other storage solutions if you already have a cloud storage account you use to save and sync your files. Simply click on the appropriate logo to access your existing data.', gtCore.pgettext('app', 'Drive')))
      .on('before:show', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', true) })
      .on('hide', function () { $('#io-ox-topbar-settings-dropdown-icon .dropdown-toggle').click(); $('#io-ox-topbar-settings-dropdown-icon').attr('forceOpen', false) })
      .waitFor('a[data-action="add-storage-account"]')
      .spotlight('a[data-action="add-storage-account"]')
      .hotspot('a[data-action="add-storage-account"]')
      .end()
      .on('stop', cleanup)
  }

  $.when(
    // #. %1$s is usually "Drive" (product name; might be customized). This string is used as a filename
    api.upload({ folder: standardFolder, file: blob, filename: gt('The %1$s app tour', gtCore.pgettext('app', 'Drive')) + '.txt' }),
    ox.launch('io.ox/files/main')
  ).then(function (fileData) {
    file = fileData
    key = _.cid(fileData)
    api.trigger('refresh.all')

    const app = ox.ui.App.getCurrentApp()
    // remember current state
    current.folder = app.folder.get()
    current.view = app.props.get('layout')

    // set folder and layout: ensure we find the uploaded 'drive app tour.txt'
    if (current.folder === standardFolder) return cont()
    return app.folder.set(standardFolder).then(cont)

    function cont () {
      if (current.view === 'tile') app.props.set('layout', 'list')
      ox.trigger('refresh^')
      tour.start()
    }
  })
})
