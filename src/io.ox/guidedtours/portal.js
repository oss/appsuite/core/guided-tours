/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import Tour from '$/io.ox/core/tk/wizard'
import gt from 'gettext'
import openSettings, { closeSettings } from '$/io.ox/settings/util'
import apps, { minimizeFloatingWindows, restoreFloatingWindows } from '$/io.ox/core/api/apps'

/* Tour: portal */
Tour.registry.add({
  id: 'default/io.ox/portal',
  app: 'io.ox/portal',
  priority: 1
}, function () {
  const tour = new Tour()

  // close all floating windows that would interfere with the tour
  const openFloatingWindows = apps.getOpenFloatingWindows()
  minimizeFloatingWindows(openFloatingWindows)

  // reopen temporary hidden floating windows
  tour.on('stop', () => restoreFloatingWindows(openFloatingWindows))

  tour.step()
    .title(gt('The Portal'))
    .content(gt('The Portal informs you about current emails, appointments or social network news.'))
    .end()
    .step()
    .title(gt('Reading the details'))
    .content(gt('To read the details, click on an entry in a square.'))
    .spotlight('.widget .item')
    .end()
    .step()
    .title(gt('Drag and drop'))
    .content(gt('To change the layout, drag a square\'s title to another position and drop it there.'))
    .spotlight('.widget:visible:first')
    .end()
    .step()
    .title(gt('Closing a square'))
    .content(gt('If you no longer want to display a square, click the delete icon.'))
    .hotspot('.widget .disable-widget svg:visible')
    .spotlight('.widget .disable-widget svg:visible')
    .end()
    .step()
    .title(gt('Customizing the Portal'))
    .content(gt('To reconfigure a square again or to display further information sources, go to the Portal section in the settings app.'))
    .on('before:show', () => openSettings('virtual/settings/io.ox/portal'))
    .on('close back next', closeSettings)
    .waitFor('.io-ox-portal-settings')
    .referTo('.io-ox-portal-settings')
    .spotlight('.rightside.settings-detail-pane')
    .hotspot('li[data-id="virtual/settings/io.ox/portal"] .folder-label')
    .end()
    .step()
    .title(gt('Add a new widget'))
    .content(gt('You can also add a new square by clicking the Add widget button'))
    .navigateTo(() => import('$/pe/portal/main'))
    .waitFor('.header .add-widget')
    .referTo('.header .add-widget')
    .hotspot('.header .add-widget')
    .spotlight('.header .add-widget')
    .end()
    .start()
})
