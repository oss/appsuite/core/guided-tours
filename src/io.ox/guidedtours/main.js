/**
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 */

import $ from '$/jquery'
import _ from '$/underscore'

import ext from '$/io.ox/core/extensions'
import Stage from '$/io.ox/core/extPatterns/stage'
import capabilities from '$/io.ox/core/capabilities'

import { gt } from 'gettext'

import { Settings } from '$/io.ox/core/settings'

export const settings = new Settings('io.ox/tours', () => ({
  widgets: {
    user: {}
  }
}))

/* New stage: Starts a tour upon login (unless it was already seen in that particular version) */
export const stage = new Stage('io.ox/core/stages', {
  id: 'tours',
  index: 1000,
  run: async function (baton) {
    // no tours for guests, they are just annoying when you receive a sharing link etc
    // tablets are fine just disable phones
    if (_.device('smartphone') || capabilities.has('guest')) return

    await settings.ensureData()

    const disableTour = settings.get('server/disableTours')
    const startOnFirstLogin = settings.get('server/startOnFirstLogin')
    const tourVersionSeen = settings.get('user/alreadySeenVersion', -1)

    if (!disableTour && startOnFirstLogin && tourVersionSeen === -1) {
      settings.set('user/alreadySeenVersion', 1).save()

      baton.data.popups.push({ name: 'tour:io.ox/intro' })
      return Promise.all([
        import('$/io.ox/core/tk/wizard'),
        import('@/io.ox/guidedtours/intro')
      ]).then(function ([{ default: Tour }]) {
        return Tour.registry.run('default/io.ox/intro')
      })
    } else if (!disableTour && !settings.get('multifactor/shownTour', false)) {
      return import('@/io.ox/guidedtours/multifactor').then(function ({ default: tour }) {
        if (tour.run()) baton.data.popups.push({ name: 'tour:io.ox/multifactor' })
      })
    }
  }
})

/* Link: Intro tour in settings toolbar */
ext.point('io.ox/core/appcontrol/right/help').extend({
  id: 'intro-tour',
  index: 210, /* close to the help link */
  extend: function () {
    if (_.device('smartphone') || settings.get('disableTours', false) || capabilities.has('!webmail || guest')) {
      // tablets are fine just disable phones
      return
    }

    this.append(
      $('<a target="_blank" href="" role="menuitem">').text(
        // #. Tour name; general introduction
        gt('Getting started')
      ).on('click', function (e) {
        e.preventDefault()
        Promise.all([
          import('$/io.ox/core/tk/wizard'),
          import('@/io.ox/guidedtours/intro')
        ]).then(function ([{ default: Tour }]) {
          Tour.registry.run('default/io.ox/intro')
        })
      })
    )

    this.$ul.find('li').last().addClass('io-ox-specificHelp')
  }
})

ext.point('io.ox/core/appcontrol/right/help').sort()
